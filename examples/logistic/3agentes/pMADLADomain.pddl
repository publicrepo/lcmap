(define
	(domain tmpDomain)
	(:requirements :typing)
	(:types truck package location city plane)
	(:predicates
		(inPlane ?p - package ?plane - plane)
		(isAirport ?locTo - location)
		(inCity ?locTo - location ?city - city)
		(atTruck ?truck - truck ?loc - location)
		(atPlane ?plane - plane ?locFrom - location)
		(atPackage ?p - package ?loc - location)
		(inTruck ?p - package ?truck - truck)
	)
	(:action loadTruck
		:parameters (?truck - truck ?p - package ?loc - location)
		:precondition
			(and
				(atTruck ?truck ?loc) (atPackage ?p ?loc)
			)

		:effect
			(and
				(inTruck ?p ?truck)
				(not(atPackage ?p ?loc))
			)
	)

	(:action unloadTruck
		:parameters (?truck - truck ?p - package ?loc - location)
		:precondition
			(and
				(atTruck ?truck ?loc) (inTruck ?p ?truck)
			)

		:effect
			(and
				(atPackage ?p ?loc)
				(not(inTruck ?p ?truck))
			)
	)

	(:action flyPlane
		:parameters (?locFrom - location ?locTo - location ?plane - plane)
		:precondition
			(and
				(isAirport ?locTo) (atPlane ?plane ?locFrom) (isAirport ?locFrom)
			)

		:effect
			(and
				(atPlane ?plane ?locTo)
				(not(atPlane ?plane ?locFrom))
			)
	)

	(:action unloadPlane
		:parameters (?loc - location ?p - package ?plane - plane)
		:precondition
			(and
				(inPlane ?p ?plane) (atPlane ?plane ?loc)
			)

		:effect
			(and
				(atPackage ?p ?loc)
				(not(inPlane ?p ?plane))
			)
	)

	(:action driveTruck
		:parameters (?truck - truck ?city - city ?locFrom - location ?locTo - location)
		:precondition
			(and
				(inCity ?locFrom ?city) (inCity ?locTo ?city) (atTruck ?truck ?locFrom)
			)

		:effect
			(and
				(atTruck ?truck ?locTo)
				(not(atTruck ?truck ?locFrom))
			)
	)

	(:action loadPlane
		:parameters (?loc - location ?p - package ?plane - plane)
		:precondition
			(and
				(atPackage ?p ?loc) (atPlane ?plane ?loc)
			)

		:effect
			(and
				(inPlane ?p ?plane)
				(not(atPackage ?p ?loc))
			)
	)
)
